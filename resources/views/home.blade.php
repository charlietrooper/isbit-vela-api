@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @php(require '/Applications/MAMP/htdocs/isbit-login/resources/views/isbitapi.php')
                        @php($isbit = new isbitapi())
                        <h1>Mi Información</h1>
                        @php($info = $isbit->userInfo())
                            <p><strong>Nombre: </strong>{{$info['name']}}</p>
                            <p><strong>Email: </strong>{{$info['email']}}</p>
                            <p><strong>Balance BTC: </strong>{{$info['accounts'][1]['balance']}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
