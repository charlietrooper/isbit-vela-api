<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require 'config.php';

class isbitapi{
    public static function markets()
    {
        $tonce=time().'000';
        $method = "GET";
        $url = "/api/v2/markets";
        $payload = $method . '|' . $url . '|access_key=' . TEST_ACCESS_KEY . '&tonce=' . $tonce;
        $hash = hash_hmac('sha256', $payload, TEST_SECRET_KET);
        $request_url = ISBIT_TEST_URL."$url?access_key=".TEST_ACCESS_KEY."&tonce=$tonce&signature=$hash";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return(json_decode($result, true));
    }


    public static function tickers()
    {
        $tonce=time().'000';
        $method = "GET";
        $url = "/api/v2/tickers";
        $payload = $method . '|' . $url . '|access_key=' . TEST_ACCESS_KEY . '&tonce=' . $tonce;
        $hash = hash_hmac('sha256', $payload, TEST_SECRET_KET);
        $request_url = ISBIT_TEST_URL."$url?access_key=".TEST_ACCESS_KEY."&tonce=$tonce&signature=$hash";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return(json_decode($result, true));
    }


    public static function tickersByMarket($market)
    {
        $markets = strtolower($market).'mxn';
        $url = "/api/v2/tickers/".$markets;
        $request_url = ISBIT_TEST_URL."$url";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return(json_decode($result, true));
    }


    public static function userInfo()
    {
        $tonce=time().'000';
        $method = "GET";
        $url = "/api/v2/members/me";
        $payload = $method . '|' . $url . '|access_key=' . TEST_ACCESS_KEY . '&tonce=' . $tonce;
        $hash = hash_hmac('sha256', $payload, TEST_SECRET_KET);
        $request_url = ISBIT_TEST_URL."$url?access_key=".TEST_ACCESS_KEY."&tonce=$tonce&signature=$hash";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return(json_decode($result, true));
    }


    public static function deposits($currency,$limit)
    {
        $tonce=time().'000';
        $method = "GET";
        $url = "/api/v2/deposits";
        $cy = strtolower($currency);
        $payload = $method . '|' . $url . '|access_key=' . TEST_ACCESS_KEY."&cy=$cy&limit=$limit". '&tonce=' . $tonce;
        $hash = hash_hmac('sha256', $payload, TEST_SECRET_KET);
        $request_url = ISBIT_TEST_URL."$url?access_key=".TEST_ACCESS_KEY."&cy=$cy&limit=$limit"."&tonce=$tonce&signature=$hash";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return(json_decode($result, true));
    }


    public static function deposit($txid)
    {
        $tonce=time().'000';
        $method = "GET";
        $url = "/api/v2/deposit";
        $payload = $method . '|' . $url . '|access_key=' . TEST_ACCESS_KEY.'&tonce=' . $tonce."&txid=$txid";
        $hash = hash_hmac('sha256', $payload, TEST_SECRET_KET);
        $request_url = ISBIT_TEST_URL."$url?access_key=".TEST_ACCESS_KEY."&tonce=$tonce&signature=$hash"."&txid=$txid";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return(json_decode($result, true));
    }


    public static function depositAddress($currency)
    {
        $tonce=time().'000';
        $method = "GET";
        $url = "/api/v2/deposit_address";
        $cy = strtolower($currency);
        $payload = $method . '|' . $url . '|access_key=' . TEST_ACCESS_KEY."&cy=$cy".'&tonce=' . $tonce;
        $hash = hash_hmac('sha256', $payload, TEST_SECRET_KET);
        $request_url = ISBIT_TEST_URL."$url?access_key=".TEST_ACCESS_KEY."&tonce=$tonce&signature=$hash"."&cy=$cy";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return(json_decode($result, true));
    }

    public static function orders($market)
    {
        $tonce=time().'000';
        $method = "GET";
        $url = "/api/v2/orders";
        $mkt = strtolower($market);
        $payload = $method . '|' . $url . '|access_key=' . TEST_ACCESS_KEY . '&tonce=' . $tonce;
        $hash = hash_hmac('sha256', $payload, TEST_SECRET_KET);
        $request_url = ISBIT_TEST_URL."$url?access_key=".TEST_ACCESS_KEY."&tonce=$tonce&signature=$hash&market=$mkt";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        $result = curl_exec($ch);
        curl_close($ch);
        return(json_decode($result, true));
    }

}




